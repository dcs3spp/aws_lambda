# Overview

## Intro

Another repository at https://gitlab.com/dcs3spp/rapport_test_dev shows a docker-compose architecture for 
a Python Flask API framework. This contains a main branch and a aws branch. 

The main branch contains a custom login form and flask API architecture running in docker-compose.

The aws branch contains code to hook up the flask endpoints to an AWS cognito identity provider in local docker-compose stack. 
Environment variables would have to be added to connect to AWS Cognito user pool as documented in the readme for the branch.

Please read the README.md files for both branches.

The other repository also demonstrates usage of:

- Gitlab CI
- pre-commit rules for flask8, black code linting and prettifier functionality. It also contains
    a prec-commit rule for linting the GitLab CI file specification. This uses my own linter devloped
    in Ruby to perform the linting. Source code can be found at https://github.com/dcs3spp/validate-gitlab-ci.
    It is available as a ruby gem at https://rubygems.org/gems/gitlab-lint-client.
- Use of SqlAlchemy and Marshmallow for DB model and JSON serialization.
- Pytest unit test samples
- Docker-compose architecture


## This repository

This repository contains source for lambdas and upload web form for use in AWS. It contains
the following folders:
- doc folder contains a pdf illustrating an architectural solution for the task.
- src folder contains the lambdas created so far and a bootstrap 4 web form.

So far I have managed to get the lambdas created and triggered and hooked up to API Gateway.
There are two endpoints:
1. **/files**: [GET] Returns JSON link to S3 files uploaded
2. **/files**: [POST] Upload json body to S3 storage. This triggers an event and stores a link to s3
file in dynamoDB.

I have not yet hosted the upload web form. What I would do is hook this up to the API gateway 
so that the gateway returns text/html content fetching this from Cloudfront or S3 storage. 
However, I would also have to investigate how to forward authorization headers when the form 
posts back to the upload endpoint.

One way to achieve this would be to setup a server only session cookie for a domain that gets 
sent to each request after login. Alternatively, the web client could store the auth PKCE code
in local storage for retrieval of the session token for use in Authorization header. 

I would have also liked to investigate having the AWS cloud resources environment set up and 
described using Amazon cloudformation template for replicability and have some unit testing 
included for the lambdas. Not sure how to achieve this for existing cloud resources?
