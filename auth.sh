#!/usr/bin/env sh

curl --location --request POST 'https://rapport-test.auth.us-east-2.amazoncognito.com/oauth2/token' \
--header 'Content-Type: application/x-www-form-urlencoded' \
--header 'Authorization: Basic MnVubnQ1aTQ4YzN0aGU5aTdsbDU2MDVtZWE6aWZwcmpxZG9yMWo5YXJmMHFtcGUwM2d1cjY4N2E0azJkbG52cWdncjc0YTN1djhza203' \
--data-urlencode 'grant_type=authorization_code' \
--data-urlencode 'client_id=2unnt5i48c3the9i7ll5605mea' \
--data-urlencode 'code=73a45a72-99b2-4556-90aa-027b9e1c82b2' \
--data-urlencode 'redirect_uri=http://localhost:8000/aws_cognito_redirect' 
#--data-urlencode 'code_verifier=d2c30ab198e3da5797682f7c622a15ff'

