import json
import logging
import os

import boto3

logger = logging.getLogger()
logger.setLevel(logging.INFO)

BUCKET_NAME_KEY = "AWS_BUCKET"


def lambda_handler(event, context):
    """ Lambda function that prepares links to objects on given bucket
    Should be extended to include use of paginators and JSONAPI format

    Parameters
    ----------
    event: dict, required
        API Gateway Lambda Proxy Input Format

        #api-gateway-simple-proxy-for-lambda-input-format
        Event doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html

    context: object, required
        Lambda Context runtime methods and attributes

        Context doc: https://docs.aws.amazon.com/lambda/latest/dg/python-context-object.html

    Environment Variables
    ---------------------
    AWS_BUCKET: The name of the bucket to read from

    Returns
    ------
    API Gateway Lambda Proxy Output Format: dict indicating 201 if successfully created.

        Return doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html
    """

    logger.info("Received event : " + json.dumps(event))

    if os.environ.get(BUCKET_NAME_KEY) == None:
        return {
            "statusCode": 500,
            "body": json.dumps({
                "status": "Internal server error: " + BUCKET_NAME_KEY + " environment variable must be set"
            })
        }

    AWS_BUCKET = os.environ.get(BUCKET_NAME_KEY)
    BASE_URL = "https://rapport-files-bucket.s3.amazonaws.com/" + AWS_BUCKET + "/"

    logger.info("Attempting to read from bucket := " + AWS_BUCKET)
    logger.info("Base url is := " + BASE_URL)

    files = []

    try:
        s3 = boto3.resource('s3')
        allFiles = s3.Bucket(AWS_BUCKET).objects.all()
        for file in allFiles:
            files.append(str(BASE_URL)+str(file.key))
    except Exception as ex:
        logging.error(
            "Exception encountered accessing s3 storage for bucket " + AWS_BUCKET, exc_info=True)

        return {
            "statusCode": 500,
            "body": json.dumps({
                "status": "Internal server error while accessing S3 bucket := " + AWS_BUCKET
            })
        }

    return {
        "statusCode": 200,
        "body": json.dumps({"data": files})
    }

