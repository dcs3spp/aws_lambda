import json
import logging
import os

import boto3

logger = logging.getLogger()
logger.setLevel(logging.INFO)

TABLE_KEY = "AWS_DYNAMODB_TABLE_NAME"


def lambda_handler(event, context):
    """ Lambda function that receives an S3 bucket put event triggered when the 
    API has posted a JSON payload to S3 storage. This event stores the id of the
    file that was created in S3 storage. The file id is stored in dynamo db table.

    Parameters
    ----------
    event: dict, required
        API Gateway Lambda Proxy Input Format

        Event doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html#api-gateway-simple-proxy-for-lambda-input-format

    context: object, required
        Lambda Context runtime methods and attributes

        Context doc: https://docs.aws.amazon.com/lambda/latest/dg/python-context-object.html

    Environment Variables
    ---------------------
    AWS_DYNAMODB_TABLE_NAME: The name of the dynamo db table

    Returns
    ------
    API Gateway Lambda Proxy Output Format: dict indicating 201 if successfully created.

        Return doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html
    """

    if os.environ.get(TABLE_KEY) == None:
        return {
            "statusCode": 500,
            "body": json.dumps({
                "status": "Internal server error: " + TABLE_KEY + " environment variable must be set"
            })
        }

    logger.info("Received event : " + json.dumps(event))

    TABLE_NAME = os.environ[TABLE_KEY]

    file_uuid = None
    try:
        file_uuid = event["Records"][0]["s3"]["object"]["key"]
    except Exception as ex:
        logger.error(
            "Exception encountered retrieving S3 file ID := ", exc_info=True
        )

        return {
            "statusCode": 500,
            "body": json.dumps({
                "status": "Internal server error due to failure to read S3 event data"
            })
        }

    logger.info(
        "Attempting to write event to DynamoDB Table := " + str(TABLE_NAME))

    try:
        client = boto3.resource('dynamodb')
        table = client.Table(TABLE_NAME)
        table.put_item(Item={"id": file_uuid})
    except Exception as ex:
        logger.error(
            "Exception encountered while writing to dynamodb := ", exc_info=True
        )

        return {
            "statusCode": 500,
            "body": json.dumps({
                "status": "Internal server error while writing to dynamoDB"
            })
        }

    return {
        "statusCode": 201,
        "body": json.dumps({
            "status": "OK",
        }),
    }

