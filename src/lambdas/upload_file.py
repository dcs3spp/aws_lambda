import json
import os
import uuid

import boto3


def lambda_handler(event, context):
    """Lambda function that uploads json body to S3 bucket accessed from the AWS_BUCKET env var

    Parameters
    ----------
    event: dict, required
        API Gateway Lambda Proxy Input Format

        # api-gateway-simple-proxy-for-lambda-input-format
        Event doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html

    context: object, required
        Lambda Context runtime methods and attributes

        Context doc: https://docs.aws.amazon.com/lambda/latest/dg/python-context-object.html

    Environment Variables
    ---------------------
    AWS_BUCKET: The name of the S3 bucket

    Returns
    ------
    API Gateway Lambda Proxy Output Format: dict

        Return doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html
    """

    AWS_BUCKET = os.environ['AWS_BUCKET']

    if os.environ.get('AWS_BUCKET') == None:
        return {
            "statusCode": 500,
            "body": json.dumps({
                "status": "Internal server error: AWS_BUCKET environment variable must be set"
            })
        }

    print("payload: ", event['body'])

    try:
        json_object = json.loads(event["body"])
    except json.decoder.JSONDecodeError as ex:
        print(ex)
        return {
            "statusCode": 400,
            "body": json.dumps({
                "status": "400",
                "message": str(e),
            })
        }

    s3 = boto3.client("s3")
    json_object = event["body"]

    print("attempting to write to bucket := ", AWS_BUCKET)

    s3.put_object(
        Body=json.dumps(json_object),
        Bucket=AWS_BUCKET,
        Key=str(uuid.uuid4())
    )

    print("Uploaded payload successfully to s3 storage")

    return {
        "statusCode": 201,
        "body": json.dumps({
            "status": "OK",
        }),
    }

